/* 
 * File:        main.c
 * Author:      Christian Gewert
 * Description: 
 * Created:     07.03.2015
 */

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include "bass.h"
#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "audiomanager.h"
#include "logmanager.h"
#include "gamelib.h"

/* Stores games desired FPS. */
int desiredFPS;
/* Stores how much usecs a game loop iteration should take. (1 sec = 10⁶ useconds) */
long looptime;
/* This flag tells if game shall terminate. */
int isGameOver;
/* Stores how much usecs game thread will sleep after a game loop iteration. */
long sleepTime;
/* Stores how much usecs game loop took. */
long loopDuration;
/* Measures time taken in a game loop iteration */
struct timeval timeStart, timeStop;
/* Stores how much usecs the game loop missed. */
long overtime;
/* Measures if a second has passed. */
long second;
/* Handle for game window. */
SDL_Window* window;
/* Handle for SDL renderer. */
SDL_Renderer* renderer;
/* Surface used for rendering text on it. */
SDL_Surface* text_surface;
/* Measures fps and ups */
int fps, ups;
/* Stores SDL Window events. */
SDL_Event e;
/* Stores state of the game. */
enum EGameState gameState;
/* Stores loaded true type font */
TTF_Font* font;
TTF_Font* intro_font;
SDL_Color fontColor;
SDL_Color intro_fontColor = {255, 0, 0, 255};
/* Represents a player object. */
struct Player player;
/* Represents a map object. */
struct Map map;
/* Lua state variable. */
lua_State *L;
/* Counts how many tiles are defined in the map script. */
int tilecount;
int *pRow = NULL;

/* Closing used libraries and free resources. */
void unload(){
    /* Free reserved memory */
    free(map.data);
    map.data = NULL;
    /* Shutting down logging to file. */
    closeGameLog();
    TTF_CloseFont(font);
    TTF_CloseFont(intro_font);
    font = NULL;
    /* Shutting down BASS */
    BASS_Free();
    /* Shutting down SDL */
    IMG_Quit();
    TTF_Quit();
    SDL_Quit();
}

/* 
 * Initializes game structures and libraries used.
 * Returns 1 if all is ok, otherwise 0.
 */
int initialize(){
    printf("Game initializing...\n");
    int isInitialized = TRUE;
    desiredFPS = 60;
    looptime = 1000000 / desiredFPS;
    isGameOver = FALSE;
    sleepTime = 0;
    loopDuration = 0;
    overtime = 0;
    second = 0;
    window = NULL;
    renderer = NULL;
    fps = 0;
    ups = 0;
    gameState = Init;
    fontColor.a = 255;
    fontColor.r = 255;
    fontColor.g = 255;
    fontColor.b = 255;
    player.position.x = -1;
    player.position.y = -1;
    map.data = NULL;
    int tile;
    for(tile=0; tile<GAME_TILE_COUNT; tile++){
        map.tiles[tile] = (char*)malloc(256*sizeof(char));
    }
    tilecount = 0;
    
    /* Start logging to file. */
    openGameLog("gamelog.txt");
    
    /* Initializing BASS Audio Engine */
    printf("Audio System initializing...");
    if ( BASS_Init( -1, 44100, 0, 0, NULL) ){
        printf("finished!\n");
        //BASS_SetVolume(0.5);
    }
    else {
        printf("failed!\n");
        isInitialized = FALSE;
    }
    
    /* Initializing SDL */
    printf("SDL initializing...");
    if ( SDL_Init(SDL_INIT_EVERYTHING) != 0 ){
        printf("failed!\n");
        isInitialized = FALSE;
    }
    else {
        /* Init SDL TTF. */
        if ( TTF_Init() != 0 ){
            isInitialized = FALSE;
        }
        /* Init SDL IMG. */
        IMG_Init(IMG_INIT_JPG);
        
        printf("finished!\n");
    }
    
    /* Initializing LUA */
    printf("Lua initializing...");
    L = luaL_newstate();
    luaL_openlibs(L);
    printf("finished!\n");
    
    return isInitialized;
}

/* Loads ressources from hdd. */
int load(){
    int retval = 1;
    printf("Loading ressources...\n");
    
    printf("Loading fonts...");
    font = TTF_OpenFont("Open_Sans/OpenSans-Regular.ttf",22);
    intro_font = TTF_OpenFont("Open_Sans/OpenSans-Bold.ttf",48);
    if(!font || !intro_font){
        retval = 0;
        printf("failed!\n");
    }
    else {
        printf("finished!\n");
    }
    
    printf("Loading map...");
    int loaded = luaL_loadfile(L, "lua/map1.lua");
    if (loaded != LUA_OK){
        printf("failed!\n");
        printf("Lua Error: %d\n", loaded);
        retval = 0;
    }
    else{
        lua_call(L, 0, 0);  // priming run
        // Fetch map params
        lua_getglobal(L,"getXStart");
        lua_pcall(L, 0, 1, 0);
        map.start.x = lua_tonumber(L, -1);
        lua_pop(L,1);
        lua_getglobal(L,"getYStart");
        lua_pcall(L, 0, 1, 0);
        map.start.y = lua_tonumber(L, -1);
        lua_pop(L,1);
        lua_getglobal(L,"getXEnd");
        lua_pcall(L, 0, 1, 0);
        map.end.x = lua_tonumber(L, -1);
        lua_pop(L,1);
        lua_getglobal(L,"getYEnd");
        lua_pcall(L, 0, 1, 0);
        map.end.y = lua_tonumber(L, -1);
        lua_pop(L,1);
        /* Player starts at start spawn. */
        player.position.x = map.start.x;
        player.position.y = map.start.y;
        lua_getglobal(L,"getXDimension");
        lua_pcall(L, 0, 1,0);
        map.dimension.x = lua_tonumber(L, -1);
        lua_pop(L,1);
        lua_getglobal(L,"getYDimension");
        lua_pcall(L, 0, 1,0);
        map.dimension.y = lua_tonumber(L, -1);
        map.data = (int**)malloc(map.dimension.y * sizeof(int*));
        lua_pop(L,1);
        /* Fetch map */
        int row;
        int key, val;
        for(row=1; row<=map.dimension.y; row++){
            *(map.data+(row-1)) = (int*)malloc(map.dimension.x * sizeof(int));
            lua_getglobal(L,"getMapRow");
            lua_pushnumber(L,row);
            lua_pcall(L, 1, 1,0);
            lua_pushnil(L); // Lua shall start at the beginning
            int col = 1;
            while (lua_next(L, -2)) {
                val = lua_tonumber(L, -1);
                int *column = *(map.data+row-1);
                *(column+col-1) = val;
                lua_pop(L,1);
                key = lua_tonumber(L, -1);
                col++;
            }
        }
        
        // Fetch map tiles
        lua_getglobal(L,"getTiles");
        lua_call(L, 0, 1);
        lua_pushnil(L); // Lua shall start at the beginning
        int k; char *v;
        while (lua_next(L, -2)) {
            v = lua_tostring(L, -1);
            lua_pop(L,1);
            k = lua_tonumber(L, -1);
            strcpy(map.tiles[k],v);
            tilecount++;
        }
        printf("finished!\n");
        int i;
        
        /* Free Lua state var. */
        lua_close(L);
    }
            
    int tex;
    for(tex=1; tex<=tilecount; tex++){
        printf("Loading texture %d = %s...", tex, map.tiles[tex]);
        //map.textures[i] = IMG_LoadTexture(renderer, map.tiles[i]);
        SDL_Surface* loadedSurface = IMG_Load( map.tiles[tex] );
        map.textures[tex] = SDL_CreateTextureFromSurface(renderer, loadedSurface);
        free(loadedSurface);
        if(map.textures[tex] == NULL){
            printf("failed!\n");
        }
        else{
            printf("finished!\n");
        }
    }
    
    // TODO: delete testing
    loadAudioStream("track1.mp3");
    playAudioStream(0);
    
    return retval;
}

/* Games update logic. */
void update(){
    while( SDL_PollEvent(&e) ){
        if ( e.type == SDL_QUIT ) { isGameOver = TRUE; }
        
        if ( e.type == SDL_KEYDOWN ) { 
            switch(e.key.keysym.sym) {
                case SDLK_q:
                    isGameOver = TRUE;
                    break;
                default:
                    break;
            }
        }
        
        /* Branch for games state specific updating. */
        switch(gameState){
            case Intro:
                /* Handle Keyboard Input. */
                if ( e.type == SDL_KEYDOWN ) { 
                    gameState = Menu;
                }
                break;
            case Menu:
                /* Handle Keyboard Input. */
                if ( e.type == SDL_KEYDOWN ) { 
                    gameState = Ingame;
                }
                break;
            case Ingame:
                if ( e.type == SDL_KEYDOWN ) { 
                    switch(e.key.keysym.sym) {
                        case SDLK_RIGHT:
                            if(!e.key.repeat){
                                player.position.x += 1;
                            }
                            break;
                        case SDLK_LEFT:
                            if(!e.key.repeat){
                                player.position.x -= 1;
                            }
                            break;
                        case SDLK_UP:
                            if(!e.key.repeat){
                                player.position.y -= 1;
                            }
                            break;
                        case SDLK_DOWN:
                            if(!e.key.repeat){
                                player.position.y += 1;
                            }
                            break;
                        default:
                            break;
                    }
                }
                break;
            case Paused:
                break;
            case Outro:
                break;
            default:
                break;
        }
    }
}

/* Draws all screen content. */
void draw(){
    /* Clear Screen to black */
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_Texture* font_texture = NULL;
    SDL_DisplayMode mode;
    int w = 0, h = 0;
    SDL_GetWindowDisplayMode(window, &mode);
    int index = 0;
    /* Draw Operations. */
    
    /* Draw state dependent screens */
    switch(gameState){
        case Intro:
            text_surface = TTF_RenderUTF8_Blended(intro_font,"0x01 hunted", intro_fontColor);
            font_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
            if (font_texture){
                SDL_QueryTexture(font_texture,NULL,NULL,&w, &h);
                SDL_Rect dest = {(mode.w/2)-w/2,(mode.h/2)-h/2,w,h};
                SDL_RenderCopy(renderer, font_texture, NULL, &dest);
            }
            SDL_FreeSurface(text_surface);
            free(font_texture);
            break;
        case Menu:
            text_surface = TTF_RenderUTF8_Blended(font,"Press any key to start a new game, press <q> to exit game at any time!", fontColor);
            font_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
            if (font_texture){
                SDL_QueryTexture(font_texture,NULL,NULL,&w, &h);
                SDL_Rect dest = {(mode.w/2)-w/2,(mode.h/2)-h/2,w,h};
                SDL_RenderCopy(renderer, font_texture, NULL, &dest);
            }
            SDL_FreeSurface(text_surface);
            free(font_texture);
            break;
        case Ingame:
            /* Check players position to determine which tile has to be rendered. */
            pRow = *(map.data+(player.position.y-1));
            index = *(pRow+(player.position.x-1));
            SDL_Texture *text = map.textures[index];
            /* Render Map */
            SDL_QueryTexture( text, NULL, NULL, &w, &h);
            SDL_Rect dest = {(mode.w/2)-w/2, (mode.h/2)-h/2, w, h};
            SDL_RenderCopy(renderer, text, NULL, &dest);
            if( text == NULL ){
                printf("TEXTURE NULL!\n");
                printf("Position: %d/%d\n", player.position.x, player.position.y);
                printf("Index: %d\n", index);
            }
            /* Render Minimap */
            /* Render Player */
            break;
        case Paused:
            break;
        case Outro:
            break;
        default:
            break;
    }
    
    /* Switch Front and Backbuffer */
    SDL_RenderPresent(renderer);
    
    /* Count fps. */
    fps++;
}

/*
 * Island of Doom by C.Gewert
 * Main Entry Point.
 */
int main(int argc, char** argv) {
    /* Initializing game */
    if ( initialize() ){
        window = SDL_CreateWindow("0x01 hunted", 1680, 0, 640, 480, SDL_WINDOW_FULLSCREEN_DESKTOP);
        renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
        if (!renderer){
            printf("SDL renderer initialization error! Game terminated.\n");
            isGameOver = TRUE;
        }
    }
    else {
        printf("Game could not being initialized, terminating.\n");
        isGameOver = TRUE;
    }
    
    /* Loading game resources from hdd. */
    if ( load() ){
        
    }
    else {
        printf("Could not load all ressources. Game aborted!\n");
        isGameOver = TRUE;
    }
    
    /* Entering the faboulus Game loop */
    while(!isGameOver){
        gettimeofday(&timeStart,0);
        if (gameState == Init){ gameState = Intro; }
        update();
        draw();
        gettimeofday(&timeStop,0);
        loopDuration = timeStop.tv_usec - timeStart.tv_usec;
        second += loopDuration;
        if (loopDuration < looptime){
            sleepTime = looptime - loopDuration;
            second += sleepTime;
            usleep(sleepTime);
        }
        else if (loopDuration > looptime){
            overtime += loopDuration - looptime;
            if (overtime >= looptime){
                
            }
        }
        if(second >= 1000000){
            second -= 1000000;
            //printf("Real FPS: %d\n", fps);
            fps = 0;
        }
    }
    
    /* Free used resources. */
    unload();
    /* Terminating game. */
    return (EXIT_SUCCESS);
}