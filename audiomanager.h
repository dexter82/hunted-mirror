/* 
 * File:   audiomanager.h
 * Author: christian
 *
 * Created on 7. März 2015, 04:32
 */

#ifndef AUDIOMANAGER_H
#define	AUDIOMANAGER_H

#include "bass.h"

/**
 * @return Detected Audio devices count.
 */
int enumerateAudioDevices();

/**
 * Loads a given audio stream from hard drive.
 * @param file String containing file name.
 * @return 0 if loading fails, otherwise Stream handle.
 */
int loadAudioStream(const char* file);

/**
 * Frees loaded audio streams resources.
 * @return TRUE if successful, else FALSE.
 */
int unloadAudioStream();

/**
 * Starts playback of loaded audio stream.
 * @param loop TRUE for looping playback, FALSE for no looping.
 */
void playAudioStream(int loop);

/**
 * Pauses loaded audio streams playback.
 */
void pauseAudioStream();

/**
 * Gets the last thrown error code.
 * @return int Bass Error Code.
 */
int getBassError();

#endif	/* AUDIOMANAGER_H */

