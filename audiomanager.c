/*
 * File:        audiomanager.c
 * Author:      Christian Gewert
 * Since:       1.0
 * Description: 
 */

#include "audiomanager.h"

HSTREAM audioStream;
int bass_error = 0;

int enumerateAudioDevices(){
    BASS_DEVICEINFO device;
    int a, count = 0;
    printf("Detecting audio devices...\n");
    for ( a=0; BASS_GetDeviceInfo( a, &device ); a++ ){
        count++;
        printf("Device[%d]: [%s]\n",a,device.name);
    }
    
    return count;
}

int loadAudioStream(const char* file){
    audioStream = BASS_StreamCreateFile( FALSE, file, 0, 0, 0);
    if ( audioStream == 0 ){
        bass_error = BASS_ErrorGetCode();
    }
    
    return audioStream;
}

int unloadAudioStream(){
    int retval = BASS_StreamFree(audioStream);
    if (retval == FALSE){
        bass_error = BASS_ErrorGetCode();
    }
    return retval;
}

void playAudioStream(int loop){
    if(loop != 0){ loop = 1; }
    if ( !BASS_ChannelPlay( audioStream, loop ) ){
        bass_error = BASS_ErrorGetCode();
    }
}

void pauseAudioStream(){
    if ( !BASS_ChannelPause(audioStream) ){
        bass_error = BASS_ErrorGetCode();
    }
}

int getBassError(){
    return bass_error;
}