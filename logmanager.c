/*
 * File:        logmanager.c
 * Author:      Christian Gewert
 * Since:       1.0
 * Description: 
 */

#include "logmanager.h"

FILE* logfile = NULL;

int gamelog(const char* text){
    int retval = 0;
    char temp[1024];
    strcpy(temp, text);
    strcat(temp,"\n");
    if (logfile){
        retval = fputs(temp,logfile);
        fflush(logfile);
    }
    
    return retval;
}

int isGameLogOpen(){
    return (logfile == NULL) ? 0 : 1;
}

int openGameLog(const char* filename){
    logfile = fopen(filename, "a");
    if( isGameLogOpen() ){
        gamelog("--- Logging started ---");
    }
    return isGameLogOpen();
}

int closeGameLog(){
    int retval = 0;
    if ( isGameLogOpen() ){
        gamelog("--- Logging ended ---");
        retval = fclose(logfile);
        logfile = NULL;
    }
    
    return retval;
}