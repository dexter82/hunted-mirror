-- map1 definition file for 0x01 hunted by C.Gewert
map = {
	[1] = { 16, 16, 9, 16, 16, 16 },
	[2] = { 16, 16, 9, 16, 16, 16 },
	[3] = {  8, 12, 5,  6, 10, 16 },
	[4] = {  9, 16, 16,16,  9, 16 },
	[5] = {  9, 16, 16,16,  2, 12 },
	[6] = {  9, 16, 16,16,  9, 16 },
	[7] = {  5,  6,  6, 6,  7, 16 }
}	-- construct the map table

xdimension = 6	-- defines maps x length
ydimension = #map -- #map gets the length of array map
xstart = 3	-- defines x-position for player spawn
ystart = 7	-- defines y-position for player spawn
xend = 3	-- defines x-position for player target
yend = 1	-- defines y-position for player target


-- print("X: " .. xdimension)
-- print("Y: " .. ydimension)

-- using pairs to iterate over the map table
--for i,v in ipairs(map) do
--  print(i .. "\n")
--  print(v)
--end

tiles = {
		[1] = "images/tile1.jpg",
		[2] = "images/tile2.jpg",
		[3] = "images/tile3.jpg",
		[4] = "images/tile4.jpg",
		[5] = "images/tile5.jpg",
		[6] = "images/tile6.jpg",
		[7] = "images/tile7.jpg",
		[8] = "images/tile8.jpg",
		[9] = "images/tile9.jpg",
		[10] = "images/tile10.jpg",
		[11] = "images/tile11.jpg",
		[12] = "images/tile12.jpg",
		[13] = "images/tile13.jpg",
		[14] = "images/tile14.jpg",
		[15] = "images/tile15.jpg",
		[16] = "images/tile18.jpg"
}	-- construct the tiles table

function getMap()
	return map
end

function getMapRow(n)
	return map[n]
end

function getTiles()
	return tiles
end

function getXDimension()
	return xdimension
end

function getYDimension()
	return ydimension
end

function getXStart()
	return xstart
end

function getYStart()
	return ystart
end

function getXEnd()
	return xend
end

function getYEnd()
	return yend
end
