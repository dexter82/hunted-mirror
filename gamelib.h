/* 
 * File:   gamelib.h
 * Author: Christian Gewert
 *
 * Created on 13. März 2015, 16:52
 */

#ifndef GAMELIB_H
#define	GAMELIB_H

// TODO: Strukturen und Typdefinitionen für alle Spieldaten hier erstellen
#define GAME_TILE_COUNT 100

/* Simple linked List type. */
typedef struct LinkedListCell{
    void* value;
    struct LinkedListCell* next;
} LinkedList;

struct DoublyLinkedList{
    
};

struct Stack{
    
};


/* Represents states in which game can be. */
enum EGameState {   Init = 0, Intro = 1, Menu = 2, Ingame = 3, 
                    Paused = 4, Outro = 5 };

// Bildschirmzustände
struct gameScreen{
    
};

struct Vector2D{
    int x;
    int y;
};

// Spieler Figur Daten
struct Player{
    struct Vector2D position;
    SDL_Texture* texture;
};

// Map Struktur
struct Map{
    struct Vector2D start;
    struct Vector2D end;
    struct Vector2D dimension;
    char *tiles[GAME_TILE_COUNT];
    SDL_Texture *textures[GAME_TILE_COUNT];
    int **data;
};

// Gegner Figur Daten
struct enemy{
    
};

#endif	/* GAMELIB_H */

