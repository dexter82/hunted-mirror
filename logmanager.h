/* 
 * File:   logmanager.h
 * Author: christian
 *
 * Created on 7. März 2015, 04:43
 */

#ifndef LOGMANAGER_H
#define	LOGMANAGER_H

#include <stdio.h>
#include <string.h>

/**
 * Writes the given String(max. 1024 bytes) into the
 * gamelog file.
 * @param text String to write into gamelog file.
 * @return 0 on failure, non 0 on success.
 */
int gamelog(const char* text);

/**
 * Checks if a logfile is opened.
 * @return 0 if log is not opened, else 1.
 */
int isGameLogOpen();

/**
 * Opens a given file as game log file.
 * The file is opened in append mode.
 * @param filename String containing file name.
 * @return 0 if file could not be opened, else 1.
 */
int openGameLog(const char* filename);

/**
 * Closes an opened game log file.
 * @return EOF if an error occured, else 0.
 */
int closeGameLog();

#endif	/* LOGMANAGER_H */

